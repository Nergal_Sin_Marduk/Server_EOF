#if defined(_WIN32)
#ifndef _WIN32_WINNT
#define _WIN32_WINNT 0x0600
#endif
#include <winsock2.h>
#include <ws2tcpip2.h>
#pragma comment(lib, "ws2_32.lib")
#else
#include <arpa/inet.h>
#include <errno.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <unistd.h>
#endif

#if defined(_WIN32)
#define ISVALIDSOCKET(s) (s != INVALID_SOCKET)
#define GETSOCKETERRNO() (WSAGetLastError())
#define CLOSESOCKET(s) closesocket(s)
#else
#define ISVALIDSOCKET(s) (s >= 0)
#define GETSOCKETERRNO() (errno)
#define CLOSESOCKET(s) close(s)
#define SOCKET int
#endif
