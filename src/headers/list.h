#ifndef _LIST_H_
#define _LIST_H_

struct user_list {
  void *next_ptr;
  void *prev_ptr;
  struct User *user;
};

static unsigned int users_sz;

struct user_list *init_list();
void add_user(struct user_list **list);
void del_user(struct user_list *list, struct User *user);
void free_list(struct user_list *list);

#endif
