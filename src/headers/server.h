#ifndef _SERVER_H_
#define _SERVER_H_

#include "../headers/list.h"
#include "crossplatform.h"
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define _WARNING_STR_WITHOUT_NAME_                                             \
  "You are not initialized in system, pls input likewise how in "              \
  "example:\n\tname:<your name>\n"

struct User {
  char Name[256];
  char Ip[256];
  SOCKET Id;
};
struct Sender_Info {
  SOCKET receiver;
  char name[256];
};
char *find_name_pattern(char *buf, unsigned buflen);
char *find_receiver_pattern(char *buf, unsigned buflen);
int handle_message(SOCKET client, fd_set *master, struct user_list *user);
SOCKET get_user_socket(char *query, unsigned int query_length,
                       struct user_list *users);
bool compare_str(char *user_input, char *user_source);
void accept_client_and_init(SOCKET client, fd_set *master, SOCKET *max_fd,
                            struct user_list **users);
char *find_username_by_socket(SOCKET client, struct user_list *lst);
#endif
