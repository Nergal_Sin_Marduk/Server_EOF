#include "headers/server.h"
#include "headers/list.h"

SOCKET get_user_socket(char *query, unsigned query_length,
                       struct user_list *users) {
  struct Sender_Info;
  char name[256];
  int i;
  SOCKET receiver = 0;
  memset(name, 0, sizeof(name));
  for (i = 0; *query != '~';) {
    name[i++] = *query++;
  }
  struct user_list *usr = users;
  while (usr) {
    if (compare_str(name, usr->user->Name)) {

      receiver = usr->user->Id;
      break;
    }
    usr = usr->prev_ptr;
  }
  ++query;
  return receiver;
}

bool compare_str(char *user_input, char *user_src) {
  for (int i = 0; i < strlen(user_input); i++) {
    if (user_input[i] != user_src[i]) {
      return false;
    }
  }
  return true;
}

char *find_name_pattern(char *buf, unsigned int buflen) {
  char *pattern = "name:";
  unsigned pattern_length = strlen(pattern);
  char *ptr = NULL;
  int match = 0, z = 0, j = 0;
  for (int i = 0; i < buflen; i++) {
    if (buf[i] == *pattern) {
      for (j = i; j < i + pattern_length;) {
        if (pattern[z++] == buf[j++]) {
          ++match;
        }
      }
      if (match == pattern_length) {
        ptr = &buf[j];
        return ptr;
      } else {
        z = match = 0;
      }
    }
  }
  return NULL;
}
char *find_receiver_pattern(char *buf, unsigned int buflen) {
  char *pattern = "send:";
  char *ptr = NULL;
  unsigned pattern_length = strlen(pattern);
  int match = 0, z = 0, j = 0;
  for (int i = 0; i < buflen; i++) {
    if (buf[i] == *pattern) {
      for (j = i; j < i + pattern_length;) {
        if (pattern[z++] == buf[j++]) {
          ++match;
        }
      }
      if (match == pattern_length) {
        ptr = &buf[j];
        return ptr;
      } else {
        z = match = 0;
      }
    }
  }
  return NULL;
}

void accept_client_and_init(SOCKET client, fd_set *master, SOCKET *max_fd,
                            struct user_list **users) {
  struct sockaddr_storage client_addr;
  socklen_t client_addrlen = sizeof(client_addr);
  SOCKET slave_sock =
      accept(client, (struct sockaddr *)&client_addr, &client_addrlen);
  if (!ISVALIDSOCKET(slave_sock)) {
    fprintf(stderr, "accept error, (%d)\n", GETSOCKETERRNO());
  }
  FD_SET(slave_sock, &(*master));
  *max_fd = *max_fd < slave_sock ? slave_sock : *max_fd;
  char buf[1024];
  if (!getnameinfo((struct sockaddr *)&client_addr, client_addrlen, buf,
                   sizeof(buf), 0, 0, NI_NUMERICHOST)) {

    printf("client connected with addr:\t%s\n", buf);
    memcpy((*users)->user->Ip, buf, sizeof((*users)->user->Ip));
    (*users)->user->Id = slave_sock;
  }
  add_user(users);
  users = (*users)->next_ptr;
}

int handle_message(SOCKET client, fd_set *master, struct user_list *users) {
  struct Sender_Info sender;
  if (find_username_by_socket(client, users) == NULL) {
    return -3;
  }
  memcpy(sender.name, find_username_by_socket(client, users),
         sizeof(sender.name));
  char *ptr = NULL;
  char recv_buffer[4096];
  memset(recv_buffer, '\0', sizeof(recv_buffer));
  int bytes = recv(client, recv_buffer, sizeof(recv_buffer), 0);
  if (bytes <= 1) {
    printf("connection closed by peer\n");
    FD_CLR(client, &(*master));
    CLOSESOCKET(client);
    return -1;
  }
  for (struct user_list *index = users; index != NULL;
       index = index->prev_ptr) {
    if (index->user->Id == client) {
      if (index->user->Name[0] == '~') {
        ptr = find_name_pattern(recv_buffer, strlen(recv_buffer));
        if (ptr == NULL) {
          send(client, _WARNING_STR_WITHOUT_NAME_,
               strlen(_WARNING_STR_WITHOUT_NAME_), 0);
        } else {
          for (int ind_n = 0; *ptr != '\0'; ind_n++) {
            index->user->Name[ind_n] = *ptr++;
          }
        }
      }
      break;
    }
  }
  if ((ptr = find_receiver_pattern(recv_buffer, strlen(recv_buffer)))) {
    sender.receiver = get_user_socket(ptr, strlen(ptr), users);
    if (!sender.receiver) {
      send(client, "usnf\n", sizeof("usnf\n"), 0);
    } else {
      while (*ptr != '~')
        ptr++;
      ++ptr;

      send(sender.receiver, ptr, strlen(ptr), 0);
      send(sender.receiver, sender.name, strlen(sender.name), 0);
    }
  } else {
    if (send(client, recv_buffer, strlen(recv_buffer), 0) < 0) {
      fprintf(stderr, "send error, (%d)\n", GETSOCKETERRNO());
      return -7;
    }
  }
  return 0;
}

char *find_username_by_socket(SOCKET client, struct user_list *lst) {
  for (struct user_list *i = lst; i != NULL; i = i->prev_ptr) {
    if (i->user->Id == client) {
      return i->user->Name;
    }
  }
  return NULL;
}
