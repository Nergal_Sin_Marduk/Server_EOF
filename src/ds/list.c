#include "../headers/list.h"
#include "../headers/server.h"
struct user_list *init_list() {
  struct user_list *first_user =
      (struct user_list *)malloc(sizeof(struct user_list));

  first_user->user = (struct User *)malloc(sizeof(struct User));
  memset(first_user->user, 0, sizeof(struct User));
  first_user->user->Name[0] = '~';
  first_user->prev_ptr = NULL;
  first_user->next_ptr = NULL;
  users_sz = 1;
  return first_user;
}
void add_user(struct user_list **list) {
  struct user_list *new_user =
      (struct user_list *)malloc(sizeof(struct user_list));
  new_user->prev_ptr = (*list);
  new_user->next_ptr = NULL;
  new_user->user = (struct User *)malloc(sizeof(struct User));
  memset(new_user->user, 0, sizeof(struct User));
  new_user->user->Name[0] = '~';
  (*list)->next_ptr = new_user;
  ++users_sz;
}
void del_user(struct user_list *list, struct User *user) {
  for (struct user_list *iterator = list; iterator->prev_ptr != NULL;
       iterator = iterator->prev_ptr) {
    if (user->Name == iterator->user->Name) {
      struct user_list *temp = iterator->next_ptr;
      if (iterator->prev_ptr == NULL) {
        free(iterator);
        list = temp;
        list->prev_ptr = NULL;
        --users_sz;
      } else {
        iterator = iterator->prev_ptr;
        free(iterator->next_ptr);
        iterator->next_ptr = temp;
        --users_sz;
      }
    }
  }
}
void free_list(struct user_list *list) {
  struct user_list *item;
  for (item = list; item->prev_ptr != NULL;) {
    struct user_list *tmp = item->prev_ptr;
    free(item);
    item = tmp;
  }
  users_sz = 0;
  free(item);
}
