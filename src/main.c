#include "headers/list.h"
#include "headers/server.h"
#include <stdio.h>
#include <string.h>
int main(int argc, char **argv) {

#if defined(_WIN32)
  WSADATA ws;
  if (WSAStartup(MAKEWORD(2, 2), &ws)) {
    fprintf(stderr, "WSAStartup error, (%d)\n", GETSOCKETERRNO());
    return -1;
  }
#endif

  struct addrinfo host_addr;
  memset(&host_addr, 0, sizeof(struct addrinfo));

  host_addr.ai_family = AF_INET;
  host_addr.ai_flags = AI_PASSIVE;
  host_addr.ai_socktype = SOCK_STREAM;

  struct addrinfo *bind_addr;

  if (argc < 2) {
    getaddrinfo(0, "6666", (struct addrinfo *)&host_addr, &bind_addr);
  } else {
    getaddrinfo(0, argv[1], (struct addrinfo *)&host_addr, &bind_addr);
  }
  SOCKET main_sock = socket(bind_addr->ai_family, bind_addr->ai_socktype,
                            bind_addr->ai_protocol);

  if (!ISVALIDSOCKET(main_sock)) {
    fprintf(stderr, "socket error, (%d)\n", GETSOCKETERRNO());
    return -2;
  }

  if (bind(main_sock, bind_addr->ai_addr, bind_addr->ai_addrlen)) {
    fprintf(stderr, "bind error, (%d)\n", GETSOCKETERRNO());
    return -3;
  }
  freeaddrinfo(bind_addr);

  if (listen(main_sock, 10)) {
    fprintf(stderr, "listen error, (%d)\n", GETSOCKETERRNO());
    return -4;
  }

  struct user_list *users = init_list();

  fd_set master;
  FD_ZERO(&master);
  FD_SET(main_sock, &master);
  SOCKET max_fd = main_sock;

  while (666) {
    fd_set fucking_slaves;
    fucking_slaves = master;

    if (select(max_fd + 1, &fucking_slaves, 0, 0, 0) < 0) {
      fprintf(stderr, "select error, (%d)\n", GETSOCKETERRNO());
      return -5;
    }
    for (int i = 1; i <= max_fd; i++) {
      if (FD_ISSET(i, &fucking_slaves)) {
        if (i == main_sock) {
          accept_client_and_init(i, &master, &max_fd, &users);
          users = users->next_ptr;
        } else {
          handle_message(i, &master, users);
        }
      }
    }
  }
  FD_CLR(main_sock, &master);
  CLOSESOCKET(main_sock);

#if defined(_WIN32)
  WSACleanup();
#endif

  return 0;
}
